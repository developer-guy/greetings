package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

const DefaultGreeting = "Hello World!"

func main() {
	// signal handler
	fmt.Println("Starting the server...")

	gm := os.Getenv("GREETING")
	if gm == "" {
		gm = DefaultGreeting
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	http.HandleFunc("/greet", greet(gm))

	go func() {
		log.Fatal(http.ListenAndServe(":8080", nil))
	}()

	<-done

	fmt.Println("Stopping the server...")
}

func greet(gm string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(fmt.Sprintf("%s", gm)))
	}
}
